package oks02;

import oks02.src.Konstanty;
import oks02.src.OsobniCislo;
import oks02.src.TypStudia;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OsobniCisloTest {
    private OsobniCislo oc;

    @BeforeEach
    void setUp() {
        oc = new OsobniCislo("Novák, Josef, fav, 2014, b, 0123, p, i");
    }


    @Test
    void osobniCislo_1() {
        assertEquals("A14B0123PI", oc.getOsobniCislo());
    }

    @Test
    void getOsobniCislo_1() {
        oc = new OsobniCislo("Kravtsov, Dmytro, fuv, 2018, u, 0245, p, i");
        assertEquals("?18?0245PI", oc.getOsobniCislo());
    }

    @Test
    void isPlatneOsobniCislo() {
        assertEquals("A14B0123PI", oc.vysledek);
    }

    @Test
    void isPlatneOsobniCislo_1() {
        oc = new OsobniCislo("Kravtsov, Dmytro, fuv, 2018, u, 0245, p, i");
        assertEquals("?18?0245PI", oc.getOsobniCislo());
    }

    @Test
    void isPlatnyFormat_1() {
        oc.platnyFormat = false;
        assertFalse(oc.isPlatnyFormat());
    }

    @Test
    void isPlatnyFormat_2() {
        oc.platnyFormat = true;
        assertTrue(oc.isPlatnyFormat());
    }

    @Test
    void getTypStudia() {
        oc.zpracujTypStudia("t");
        assertEquals(TypStudia.NEPLATNY, oc.getTypStudia());
    }

    @Test
    void getFakulta() {
        oc.zpracujFakulta("fav");
        assertEquals("A", oc.getFakulta());
    }

    @Test
    void zpracujPrijmeni() {
        oc.zpracujPrijmeni(null);
        assertEquals(Konstanty.ZNAK_CHYBY, oc.prijmeni);
    }
    @Test
    void zpracujPrijmeni_1() {
        oc.zpracujPrijmeni("BUBEN");
        assertEquals("BUBEN", oc.prijmeni);
    }
    @Test
    void zpracujPrijmeni_2() {
        oc.zpracujPrijmeni("Kravtsov");
        assertFalse(oc.platnyFormat);
    }

    @Test
    void zpracujJmeno() {
        oc.zpracujJmeno("DMYTRO");
        assertEquals("Dmytro", oc.jmeno);
    }

    @Test
    void zpracujJmeno_1() {
        oc.zpracujJmeno(null);
        assertEquals(Konstanty.ZNAK_CHYBY, oc.jmeno);
    }

    @Test
    void zpracujJmeno_2() {
        oc.zpracujJmeno(null);
        assertFalse(oc.platnyFormat);
    }

    @Test
    void zpracujRokNastupu() {
        oc.zpracujRokNastupu("2019");
        assertEquals("19", oc.rokNastupu);
    }

    @Test
    void zpracujRokNastupu_1() {
        oc.zpracujRokNastupu(null);
        assertEquals(Konstanty.ZNAK_CHYBY, oc.rokNastupu);
    }

    @Test
    void zpracujRokNastupu_Format() {
        oc.zpracujRokNastupu(null);
        assertFalse(oc.platnyFormat);
    }

    @Test
    void zpracujRokNastupu_Exception() {
        assertThrows(java.lang.NumberFormatException.class, () -> oc.zpracujRokNastupu("2g18"));
    }

    @Test
    void chybnyRokNastupu_Format() {
        oc.chybnyRokNastupu();
        assertFalse(oc.platnyFormat);
    }

    @Test
    void chybnyRokNastupu_Error() {
        oc.chybnyRokNastupu();
        assertEquals(Konstanty.ZNAK_CHYBY, oc.rokNastupu);
    }

    @Test
    void zpracujFakulta_Error() {
        oc.zpracujFakulta("fkd");
        assertEquals(Konstanty.ZNAK_CHYBY, oc.fakulta);
    }

    @Test
    void zpracujFakulta_Format() {
        oc.zpracujFakulta("fav");
        assertTrue(oc.platnyFormat);
    }

    @Test
    void zpracujFakulta() {
        oc.zpracujFakulta("fuv");
        assertEquals("A", oc.fakulta);
    }

    @Test
    void zpracujTypStudia_Error() {
        oc.zpracujTypStudia("u");
        assertEquals(TypStudia.NEPLATNY, oc.typStudia);
    }

    @Test
    void zpracujTypStudia_Format() {
        oc.zpracujTypStudia("b");
        assertTrue(oc.platnyFormat);
    }

    @Test
    void zpracujTypStudia() {
        oc.zpracujTypStudia("b");
        assertEquals(TypStudia.BAKALARSKY, oc.typStudia);
    }

    @Test
    void zpracujFormaStudia() {
        oc.zpracujFormaStudia("p");
        assertEquals(Konstanty.PLATNE_FORMY_STUDIA[0], oc.formaStudia);
    }

    @Test
    void zpracujFormaStudia_Format() {
        oc.zpracujFormaStudia("u");
        assertFalse(oc.platnyFormat);
    }

    @Test
    void zpracujFormaStudia_Error() {
        oc.zpracujFormaStudia("u");
        assertEquals(Konstanty.ZNAK_CHYBY, oc.formaStudia);
    }

    @Test
    void zpracujNepovinne_1() {
        oc.zpracujNepovinne("i");
        assertEquals("I", oc.nepovinne);
    }

    @Test
    void zpracujNepovinne_2() {
        oc.zpracujNepovinne("j");
        assertEquals(Konstanty.PRAZDNY, oc.nepovinne);
    }

    @AfterEach
    void tearDown() {
        oc = null;
    }
}