package oks05;

import oks05.src.Konstanty;
import oks05.src.OsobniCislo;
import oks05.src.TypStudia;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.Collator;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class OsobniCisloTest_PozitivniFinalni {
    private OsobniCislo oc;

    @BeforeEach
    void setUp() {
        this.oc = new OsobniCislo("Novák, Josef, fav, 2014, b, p");
        this.oc.generujOsobniCislo("0123");
    }

    @Test
    void compareTo_1() {
        assertEquals(Collator.getInstance(new Locale("cs", "CZ")).compare("Novák", "Kravtsov"),
                oc.compareTo(new OsobniCislo("Kravtsov, Dmytro, fav, 2018, b, p")),
                "Chyba: Vysledek porovnani dat osob je chybny");
    }

    @Test
    void compareTo_2() {
        assertEquals(Collator.getInstance(new Locale("cs", "CZ")).compare("Josef", "Dmytro"),
                oc.compareTo(new OsobniCislo("Kravtsov, Dmytro, fav, 2018, b, p")),
                "Chyba: Vysledek porovnani dat osob je chybny");
    }

    @Test
    void getOsobniCislo() {
        assertEquals("A14B0123P", oc.getOsobniCislo(),
                "Chyba: Vysledek gettru se nerovana ocekavanemu");
    }

    @Test
    void isPlatneOsobniCislo() {
        assertTrue(oc.isPlatneOsobniCislo(), "Chyba: Osobni cislo neni platne");
    }

    @Test
    void isPlatnyFormat() {
        assertTrue(oc.isPlatnyFormat(), "Chyba: Osobni cislo neni v platnem formatu");
    }

    @Test
    void getTypStudia() {
        assertEquals(TypStudia.BAKALARSKY, oc.getTypStudia(),
                "Chyba: Typ studia neopivida ocekavanemu");
    }

    @Test
    void getFakulta() {
        assertEquals("A", oc.getFakulta(),
                "Chyba: Fakulta neodpovida ocekavane");
    }

    @Test
    void testToString() {
        assertEquals("A14B0123P <= NOVÁK Josef", oc.toString(), "Chyba: Textova reprezentace OC neni spravna");
    }

    @Test
    void chybnyRokNastupu() {
        assertEquals("14", oc.rokNastupu,
                "Chyba: Rok nastupu neni validni");
    }

    @Test
    void naplnAtributy() {
        oc.naplnAtributy("Novák, Josef, fav, 2014, b, p");
        assertNotNull(oc.getOsobniCislo());
    }
}
