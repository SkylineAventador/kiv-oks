package oks05;

import oks05.src.Konstanty;
import oks05.src.OsobniCislo;
import org.junit.jupiter.api.Test;

import java.text.Collator;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

class OsobniCisloTest_Negativni {
    private OsobniCislo oc;

    @Test
    void testToString_1() {
        oc = new OsobniCislo("Novák, Josef, hfr, 2014, h, p");
//        assertEquals("?14?xxxxP <= NOVÁK Josef (" + Konstanty.TEXT_CHYBNY_FORMAT + ")", oc.toString(),
//                "Chyba: Textova reprezentace OC neni spravna");
        assertNotEquals("A14BxxxxP <= NOVÁK Josef", oc.toString(),
                "Chyba: Textova reprezentace OC neni spravna");

    }

    @Test
    void testToString_2() {
        oc = new OsobniCislo("Novák, Josef, fav, 2014, h, p");
        assertEquals("A14?xxxxP <= NOVÁK Josef ("
                        + Konstanty.TEXT_CHYBNY_FORMAT
                        + ")", oc.toString(),
                "Chyba: Textova reprezentace OC neni spravna");
    }

    @Test
    void compareTo() {
        oc = new OsobniCislo("Novák, Josef, fav, 2014, b, p");
        int vysledek = 0;
        int vysledek_2 = 1;
        assertEquals(vysledek_2, oc.compareTo(new OsobniCislo(
                        "Novák, Dmytro, fav, 2018, b, p")),
                "Chyba: vysledek porovnani neni spravny");
    }

    @Test
    void isPlatneOsobniCislo_1() {
        oc = new OsobniCislo("Novák, Josef, fav, 2014, h, p");
        assertFalse(oc.isPlatneOsobniCislo(),
                "Osobni cislo je platne i pri neplatnych datech");
    }

    @Test
    void isPlatneOsobniCislo_2() {
        oc = new OsobniCislo("Novák, Josef, fav, 2014, b, p");
        oc.vysledek = oc.vysledek + "xxxx";
        assertFalse(oc.isPlatneOsobniCislo(),
                "Osobni cislo je platne i pri neplatnych datech");
    }

    @Test
    void zpracujPrijmeni() {
        oc = new OsobniCislo("Novák," + null + ", fav, 2014, b, p");
        oc.zpracujPrijmeni(null);
        assertEquals(Konstanty.ZNAK_CHYBY, oc.prijmeni,
                "Chyba: Prijmeni je platne i pri nevalidnich datech");
    }

    @Test
    void chybnyRokNastupu() {
        oc = new OsobniCislo("Novák, Josef, fav," + null + ", b, p");
        //oc.zpracujRokNastupu(null);
        assertEquals(Konstanty.ZNAK_CHYBY,oc.rokNastupu,
                "Chyba: Rok nastupu je platny i pri nevalidnich datech");
    }

    @Test
    void zpracujFormaStudia() {
        oc = new OsobniCislo("Novák, Josef, fav, 2014, b," + null);
        assertEquals(Konstanty.ZNAK_CHYBY, oc.formaStudia,
                "Chyba: forma studia je platna i pri prazdnych datech");
    }

    @Test
    void zpracujJmeno() {
        oc = new OsobniCislo("Novák, " + null + ", fav, 2014, b, p");
        oc.zpracujJmeno(null);
        assertEquals(Konstanty.ZNAK_CHYBY, oc.jmeno,
                "Chyba: Jmeno je platne i pri prazdnych datech");
    }

    @Test
    void zpracujRokNastupu_1() {
        oc = new OsobniCislo("Novák, Josef, fav, 201, b, p");
        assertEquals(Konstanty.ZNAK_CHYBY, oc.rokNastupu,
                "Chyba: rok nastupu je platnych i pri 3 mistni hodnote");
    }

    @Test
    void zpracujRokNastupu_2() {
        oc = new OsobniCislo("Novák, Josef, fav, dvaTisiceCtrnact, b, p");
        assertEquals(Konstanty.ZNAK_CHYBY, oc.rokNastupu,
                "Chyba: rok nastupu je platnych i pri textove vstupni hodnote");
    }

    @Test
    void zpracujRokNastupu_3() {
        oc = new OsobniCislo("Novák, Josef, fav, 2014, b, p");
        oc.zpracujRokNastupu(null);
        assertEquals(Konstanty.ZNAK_CHYBY, oc.rokNastupu,
                "Chyba: rok nastupu je platnych i pri textove vstupni hodnote");
    }

    @Test
    void naplnAtributy_1() {
        oc = new OsobniCislo("Novák, Josef, fav, 2014, b, p");
        oc.naplnAtributy(null);
        assertNotEquals("A14BXXXXP", oc.getOsobniCislo());
    }

    @Test
    void naplnAtributy_2() {
        oc = new OsobniCislo("Novák, Josef, fav, 2014, b, p");
        oc.naplnAtributy("");
        assertNotEquals("A14BXXXXP", oc.getOsobniCislo());
    }

    @Test
    void naplnAtributy_3() {
        oc = new OsobniCislo("Novák, Josef, fav, 2014, b, p");
        oc.naplnAtributy("Novák, Josef, fav, 2014, b, p, random, string, for, this, task");
        assertNotEquals("A14BXXXXP", oc.getOsobniCislo());
    }

}