package oks05;

import oks05.src.Konstanty;
import oks05.src.OsobniCislo;
import oks05.src.TypStudia;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.Collator;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

class OsobniCisloTest_PozitivniPolotovar {
    private OsobniCislo oc;

    @BeforeEach
    void setUp() {
        this.oc = new OsobniCislo("Novák, Josef, fav, 2014, b, p");
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void compareTo_1() {
        assertEquals(Collator.getInstance(new Locale("cs", "CZ")).compare("Novák", "Kravtsov"),
                oc.compareTo(new OsobniCislo("Kravtsov, Dmytro, fav, 2018, b, p")),
                "Chyba: Vysledek porovnani dat osob je chybny");
    }

    @Test
    void compareTo_2() {
        assertEquals(Collator.getInstance(new Locale("cs", "CZ")).compare("Josef", "Dmytro"),
                oc.compareTo(new OsobniCislo("Kravtsov, Dmytro, fav, 2018, b, p")),
                "Chyba: Vysledek porovnani dat osob je chybny");
    }

    @Test
    void getOsobniCislo() {
        assertEquals("A14BxxxxP", oc.getOsobniCislo(),
                "Chyba: Vysledek gettru se nerovana ocekavanemu");
    }

    @Test
    void isPlatneOsobniCislo() {
        oc.generujOsobniCislo("0123");
        assertTrue(oc.isPlatneOsobniCislo(), "Chyba: Osobni cislo neni platne");
    }

    @Test
    void isPlatnyFormat() {
        assertTrue(oc.isPlatnyFormat(), "Chyba: Osobni cislo neni v platnem formatu");
    }

    @Test
    void getTypStudia() {
        assertEquals(TypStudia.BAKALARSKY, oc.getTypStudia(),
                "Chyba: Typ studia neopivida ocekavanemu");
    }

    @Test
    void getFakulta() {
        assertEquals("A", oc.getFakulta(),
                "Chyba: Fakulta neodpovida ocekavane");
    }

    @Test
    void naplnAtributy_1() {
        assertEquals("A14BxxxxP", oc.getOsobniCislo(),
                "Chyba: Osobni cislo neni spravne naplneno atributy");
    }

    @Test
    void naplnAtributy_2(){
        assertTrue(oc.isPlatnyFormat(),
                "Chyba: Osobni cislo neni ve spravnem formatu po naplneni atributy");
    }

    @Test
    void zpracujPrijmeni_1() {
        assertEquals("NOVÁK", oc.prijmeni,
                "Chyba: zpracovane prijmeni neni spravne, " +
                        "nebo je ve spatnem formatu");
    }

    @Test
    void zpracujPrijmeni_2() {
        assertTrue(oc.isPlatnyFormat(), "Chyba: Platny format je negativni i pri spravnem formatu jmena.");
    }

    @Test
    void zpracujPrijmeni_3() {
        assertNotEquals(Konstanty.ZNAK_CHYBY, oc.prijmeni, "Chyba: Zpracovane prijmeni ma chybny vysledek");
    }

    @Test
    void zpracujJmeno_1() {
        assertEquals("Josef", oc.jmeno,
                "Chyba: zpracovane jmeno neni spravne, " +
                        "nebo je ve spatnem formatu");
    }

    @Test
    void zpracujJmeno_2() {
        assertNotEquals(Konstanty.ZNAK_CHYBY, oc.jmeno,
                "Chyba: Zpracovane jmeno ma chybny vysledek");
    }

    @Test
    void zpracujRokNastupu() {
        assertEquals("14", oc.rokNastupu, "Chyba: Hodnota roku nastupu neni ve spravna");
    }

    @Test
    void zpracujFakulta() {
        assertEquals("A", oc.fakulta, "Chyba: Zpracovana hodnota fakulty neni spravna");
    }

    @Test
    void zpracujTypStudia() {
        assertEquals(TypStudia.BAKALARSKY, oc.getTypStudia(), "Chyba: Zpracovany typ studia neni spravny");
    }


    @Test
    void zpracujFormaStudia() {
        assertEquals("P", oc.formaStudia, "Chyba: Zpracovana hodnota formy studia neni spravna");
    }

    @Test
    void zpracujNepovinne_1() {
        assertEquals("", oc.nepovinne, "Chyba: Zpracovana nepovinna hodnota neni prazdna");
    }

    @Test
    void zpracujNepovinne_2() {
        oc.zpracujNepovinne("I");
        assertEquals("I", oc.nepovinne, "Chyba: Zpracovana nepovinna hodnota neodpovida ocekavane");
    }

    @Test
    void testToString() {
        assertEquals("A14BxxxxP <= NOVÁK Josef", oc.toString(), "Chyba: Textova reprezentace OC neni spravna");
    }
}